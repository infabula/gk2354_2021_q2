
def plus(first, second):
    total = first + second
    return total

result = plus(5, 6)
plus(first="hallo", second="wereld") # named arguments
plus(second="wereld", first="hallo") # named arguments
#print(result)

def full_name(first_name, last_name="anonymous"):  # default arguments
    return first_name + " " + last_name

print(full_name("Guido", "van Rossum"))
print(full_name("Guido"))

def add(first, second, third=0, fourth=0):
    return first + second + third + fourth

add(4, 6)
add(5, 7, 3)

def add_all(first, second, *args):
    print("first:", first)
    print("second:", second)
    print("args:", args)
    total = first + second
    for arg in args:
        total += arg
    return total

print(add_all(4, 5, 7, 9, 0, 3, 6))
'''
first: 4
second: 5
args: (7, 9, 0, 3, 6)
34
'''

def the_arguments(*args, **kwargs):
    print("Positionele argumenten:")
    for arg in args:
        print(arg)

    print("named arguments")
    for key, value in kwargs.items():
        print(key, "->", value)

'''
the_arguments(3, "Jeroen", foo="Bar", test=True)
Positionele argumenten:
3
Jeroen
named arguments
foo -> Bar
test -> True
'''
def pos_args(*args, **kwargs):
    print("Positionele argumenten:")
    for arg in args:
        print(arg)

'''
>>> dictje = {
...     'name': 'Guido',
...     'age': 42 }
>>> the_arguments(dictje)
Positionele argumenten:
{'name': 'Guido', 'age': 42}
named arguments
>>> the_arguments(**dictje)
Positionele argumenten:
named arguments
name -> Guido
age -> 42
>>> the_arguments(name="Guido", age=42)
Positionele argumenten:
named arguments
name -> Guido
age -> 42

'''