def process():
    try:
        nums = [3, 5, 7]
        while True:
            try:
                index = int(input("Geef een getal"))
                print(nums[index])
                break
            except ValueError:
                print("Dat is geen integer")
            except IndexError:
                print("Graag kleiner dan ", len(nums))

        with open('niet_bestaand.txt', 'r'):
            print("file geopend!")


    except FileNotFoundError:
        print("De file bestaat niet.")
    except Exception as e:
        print("Hoops, daar gaat wat fout")
        print(type(e))
    finally:
        print("Dit wordt altijd gevoerd")

process()

