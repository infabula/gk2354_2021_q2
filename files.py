def read_file_content():
    infile = open('argumenten.py', 'r')
    # een file is een lijst van regels
    content = infile.read()
    infile.close()
    print(content)

def read_lines():
    infile = open('argumenten.py', 'r')
    # een file is een lijst van regels
    line_counter = 1
    for line in infile:
        print(line_counter, line.rstrip())
        line_counter += 1

    infile.close()


def read_lines_with_line_count():
    infile = open('argumenten.py', 'r')
    # een file is een lijst van regels

    for loop_counter, line in enumerate(infile, 1):
        print(loop_counter, line.rstrip())

    infile.close()

def write_file():
    outfile = open('names.txt', 'w')
    outfile.write('Jeroen\n')
    outfile.write('Guido\n')

    outfile.close()


# ---------------
def file_open_with_auto_close():
    # sluit automatisch de infile als het with-blok klaar is
    with open('names.txt') as infile:
        for line in infile:
            print(line.rstrip())


#read_file_content()
#read_lines()
#read_lines_with_line_count()

#write_file()
file_open_with_auto_close()


# -------------------
import os
import os.path

'''
os.listdir()
os.mkdir()  een enkele map
os.makedirs() dieperliggende mappenstructuur
os.path.isfile()
os.path.isdir()
os.path.join()
'''

# timestamps
import datetime
'''
>>> now = datetime.datetime.now()
>>> now
datetime.datetime(2021, 4, 9, 11, 43, 11, 911964)
>>> timestamp_1 = datetime.datetime.now()
>>> timestamp_1
datetime.datetime(2021, 4, 9, 11, 44, 6, 615449)
>>> today = datetime.datetime(2021, 4, 9)
>>> today.year
2021
>>> today.day
9
>>> timestamp_1.hour
11
>>> timestamp_1.minutes
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'datetime.datetime' object has no attribute 'minutes'
>>> timestamp_1.minute
44
>>> now = datetime.datetime.now()
>>> datetime.datetime.now()
datetime.datetime(2021, 4, 9, 11, 46, 44, 207102)
>>> datetime.datetime.now()
datetime.datetime(2021, 4, 9, 11, 46, 46, 257765)
>>> datetime.datetime.now()
datetime.datetime(2021, 4, 9, 11, 46, 47, 478923)
>>> datetime.datetime.now()
datetime.datetime(2021, 4, 9, 11, 46, 48, 519728)
>>> datetime.datetime.now()
datetime.datetime(2021, 4, 9, 11, 46, 49, 460728)
>>> timestamp_1
datetime.datetime(2021, 4, 9, 11, 44, 6, 615449)
>>> timestamp_2 = datetime.datetime.now()
>>> timestamp_2
datetime.datetime(2021, 4, 9, 11, 47, 24, 271219)
>>> time_delta = timestamp_2 - timestamp_1
>>> time_delta
datetime.timedelta(seconds=197, microseconds=655770)
>>> time_delta.seconds
197
>>> now = datetime.datetime.now()
>>> filename = 'order_{year}_{month}_{day}'.format(year=now.year, month=now.month, day=now.day)
>>> filename
'order_2021_4_9'
>>> 


'''
