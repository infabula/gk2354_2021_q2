first_name = "Guido"
last_name = 'van Rossum'
age = 42

message = first_name + " " + last_name + "is " + str(age) + " jaar oud"
print(message)

# format functie van str
# template
next_message = '{2} {1} is {0} {0} jaar oud'.format(first_name, last_name, age)
print(next_message)

final_message = '{name} {last:^20} is {age} jaar oud'.format(name=first_name,
                                                             last=last_name,
                                                             age=age)
print(final_message)