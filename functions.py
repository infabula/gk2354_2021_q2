import sys
import user_interact


# VERDEEL EN HEERS!
# DRY: Don't Repeat Yourself
# descriptief (beschrijft WAT je wilt doen) vs procedureel (beschrijft HOE het gedaan wordt)

def print_line(length):
    line = '-' * length
    print(line)

def print_hello():
    """Writes a hello message to the stdout."""
    print("Hallo daar.")

def main():
    print("Later we beginnen")
    print_line(40)

    for count in range(3):
        print_hello()

    print_line(20)

    if user_interact.wants_to_quit():
        print("Gebruiker wil eindigen")
    else:
        print("Maar wij eindigen zelf toch wel")
    sys.exit(0)


# Start het programma
if __name__ == "__main__":
    main()
