words = ['foo', 'bar', 'test', 'foo', 'test']

# print even alle woorden
# for loops als je vooraf weet hoe vaak je maximaal loopt
for word in words:
    if word == 'foo':
        continue
    if word == 'test':
        break
    print('-', word)

word = input("Welk woord verwijderen\n")

# while voor vooraf onbekend aantal loops
while word in words:
    print("Verwijderen van ", word)
    words.remove(word)
else:
    print(word, " is uit de lijst")

print(words)

