# JSON Javascipt Object Notation
import json


menu = {
        'margarita': 595,
        'tuna': 750,
        'penne': 995,
}

json_content = json.dumps(menu, indent=2)
print(json_content)

#with open('menu.json', 'w') as outfile:
#    outfile.write(json_content)

# inlezen
with open('menu.json', 'r') as infile:
    data = json.loads(infile.read())

for name, price in data.items():
    print(name, price)
