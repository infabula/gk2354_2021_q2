
# list
names = ['John', 'Peter', 'Ella']

# tuple : immutable list
days_of_week = ('Maandag',
                'Dinsdag',
                'Woensdag',
                'Donderdag',
                'vrijdag',)


# Alles werkt!

phones = { 'Jeroen': '12345678',
            'Bram': '65464578',
            'Ellen': '54325455',
        }

a_set = { "foo", "fo"}