
class Person:
    def __init__(self, name, age=0):
        # properies
        self.name = name
        self.set_age(age)

    # getter
    def get_age(self):
        print("get_age wordt aangeroepen")
        return self._age

    # setter
    def set_age(self, age):
        print("set_age wordt aangeroepen")
        if age < 0:
            raise ValueError("age can't be negative")
        else:
            self._age = age

    age = property(fget=get_age, fset=set_age)

    # method
    def greet(self):
        print("Hallo ik ben", self.name)

if __name__ == "__main__":
    jeroen = Person("Jeroen", 42)
    mario = Person("Mario", 23)
    jeroen.greet()
    mario.greet()
else:
    print(__name__, "wordt geimporteerd.")
