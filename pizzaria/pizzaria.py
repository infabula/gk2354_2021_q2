"""Pizzaria programma.

Zou het niet mooi zijn als...
Begin: wens om met meerdere mensen pizza te eten
we hebben een service
Geslaald Einde: de juiste pizza's ontvangen

Onnozel
- pizza wordt niet opgehaald door ons

Excepties:
- geen geld
- buiten openingstijden
- ingredientengebrek
- oven kapot
-



"""
import sys
import os
import os.path
import datetime
from person import Person


def cent_to_euro(cents):
    euro_value = cents / 100
    return euro_value

def show_pizza_choices(menu):
    """Show a nice list of all the pizzas and prices."""
    print("Showing pizza choices")
    # loopje met for.. in menu
        # prijs ontfutselen
        # prijs omkatten naar float waard: str.format()
    #for item in menu:
    #    print(item)  # print enkel de key

    for name, cent_price in menu.items():  # geeft key en de value in een keer
        price = cent_to_euro(cent_price)
        line = '- {pizza_name:<15}{price:>6.2f} Euro'.format(pizza_name=name, price=price)
        print(line)

def ask_user_positive_integer():
    """Ask user for positive integer"""
    # in een loop: while True
    #   vraag gebruiker om getal: input()
    #   string naar integer
    #   test of de waarde groter is dan 0
    #   zo ja, retourneer dit
    #   zo nee, geef een melding
    while True:
        try:
            user_input = int(input("Hoeveel?\n"))
            amount = int(user_input)
            if amount > 0:
                return amount
            else:
                print("Geef een positief getal.")

        except Exception:
            print("Een getal graag.") # feedback

def choose_pizza(menu):
    """Selecteer een pizza uit de lijst
    en retourneer de keuze.
    """
    print("Choosing pizza")

    pizza = None
    # in een while loop
    #   vraag de gebruiker om de naam van de pizza
    #   test of de naam als key voorkomt in menu dict: 'margarita' in menu
    #   als dat het geval is: pizza, en break uit de while
    #   en anders geef een melding dat die niet bestaat
    while True:
        user_input = input("Welke pizza wens je?\n").strip()
        if user_input in menu:
            pizza = user_input
            break
        else:
            print("Kies een pizza van de lijst. ")

    amount = ask_user_positive_integer()
    print("De gebruiker wil ", amount, pizza)

    choice = (pizza, amount)  # fixtures
    return choice


def choose_pizzas(menu, fixture=False):  # default argument

    if fixture:
        return [('tuna', 5), ('margarita', 2)] # fixture

    choice = []

    # vraag een aantal keer pizza en aantal
    # en stop het resultaat in de lijst choice: list.append
    # hoe kan de gebruiker aangeven dat die klaar is?
    # yes/ no vraag : indien no: return choice
    while True:
        single_choice = choose_pizza(menu)
        choice.append(single_choice)

        user_input = input("Nog meer bestellen? (ja/nee)\n").strip().lower()
        if user_input == 'nee':  # laat een functie de keuze afhandelen
            return choice


def calculate_total_price(choice, menu):
    """
    choice heeft de structuur: [ ('tuna', 5), ('margarita', 2) ]
    menu: menu_dict = {
        'margarita': 595,
        'tuna': 750,
        'penne': 995,
    }
    de som aantal keer pizza vermenigvuldigd met prijs per stuk
    totaalprijs = 0
    for single_choice in choice
        pizzanaam en aantal uit single choice halen
        zoek de stukprijs van de pizzanaam in dictionary menu
        subtotaal = stukprijs maal aanal
        tel het het subtotaal op bij de totaalprijs

    return de totaalprijs
    """
    total_price = 0

    for single_choice in choice:
        pizza_name = single_choice[0]  # eerste positie in de tuple
        amount = single_choice[1]  # tweede positie in de tuple
        price = menu[pizza_name]  # value van de dictionary key

        sub_total = amount * price
        #total_price = total_price + sub_total
        total_price += sub_total  # add

    return total_price


def get_pizzas(money, the_choice, menu):
    """Get's some pizzas."""
    total_cent_price = calculate_total_price(the_choice, menu)
    total_euro_price = cent_to_euro(total_cent_price)
    print("De totaalprijs is", total_euro_price)  # view

    if money < total_cent_price:
        print("Je hebt te weinig geld.")
        sys.exit(1)

    pizzas = the_choice
    change = money - total_cent_price
    return pizzas, change

def write_order_to_file(order, filebase):
    """ Schrijft de order als csv naar een bestand.
    - Open de file
    - for loop over order lijst:
        - schrijf de pizza naam en het aantal weg met een comma er tussen
    - sluit de file (of automatisch met een 'with' blok

    """
    ''' 
    opslaan in de map 'orders', als deze niet bestaat, dan moeten we die aanmaken
    filebase krijgt nog een timestamp toegevoegd + extensie .csv
    '''
    order_folder = 'orders'
    if not os.path.exists(order_folder):
        try:
            os.mkdir(order_folder)
        except Exception:
            print("De folder ", order_folder, "kan niet gemaakt worden.")
            sys.exit(33)

    now = datetime.datetime.now()
    filename = '{filebase}_{y}_{m:02d}_{d:02d}_{h:02d}:{min:02d}.csv'.format( filebase=filebase,
                                                          y=now.year,
                                                          m=now.month,
                                                          d=now.day,
                                                          h=now.hour,
                                                          min=now.minute
    )

    filename = os.path.join(order_folder, filename)

    with open(filename, 'w') as outfile:
        for name, amount in order:  # pizza_data: ('margarita', 4)
            line = '{pizza_name}, {amount}\n'.format(pizza_name=name,
                                                   amount=amount)
            outfile.write(line)
        # outfile sluit automatisch in een with-block :)

    return filename  # handig om te retourneren

def main():
    try:
        print("Running pizzaria app")

        money = int(input("Je geld in centen"))# cent
        menu_list = [
            ['Margarita', 595],
            ['tuna', 750],
            ['penne', 995]
        ]  # list, dictionary

        menu_dict = {
            'margarita': 595,
            'tuna': 750,
            'penne': 995,
        }

        show_pizza_choices(menu_dict)
        choice = choose_pizzas( menu=menu_dict, fixture=True)  # named arguments
        ordered_pizzas, change = get_pizzas(money=money, the_choice=choice, menu=menu_dict)
        print("we got the pizzas:", ordered_pizzas)
        print("We received change:", cent_to_euro(change))
        filename = "pizza_order"
        write_order_to_file(ordered_pizzas, filename)
    except Exception as e:
        print("Er is iets grandioos mis gegaan.")
        print(e)
        sys.exit(1)

    print("Finished")
    sys.exit(0)

if __name__ == "__main__":
    main()