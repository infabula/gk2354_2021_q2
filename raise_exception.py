def ask_number():
    """ asks the user maximal 3 times for a number.
    Raises ValueError if more than 3 tries.
    """
    tries = 0
    while True:
        try:
            num = int(input("Een getal\n"))
            return num
        except Exception:
            tries += 1
            print("Dat is geen getal")
            if tries > 3:
                raise ValueError("De gebruiker is besluiteloos")

try:
    result = ask_number()
    print(result)
except ValueError as e:
    print("De kat zit op het toetsenbord.")
    print(e)