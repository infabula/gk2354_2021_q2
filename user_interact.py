import sys


def wants_to_quit():
    """Asks the user to quit.

    Returns True or False.
    """
    while True:
        user_input = input("Wil je stoppen?\n")
        if user_input == 'ja' or user_input == 'yes':
            return True
        if user_input == 'nee':
            return False