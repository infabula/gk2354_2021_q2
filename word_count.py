user_input = input("Please enter the path and name of the text file you want"
                   " to analyze. (E.g.: C:/Users/Monty/Desktop/file.txt):"
                   "\n")

# Sending user's input back to them, for now.
print(user_input)

common_word = input("Would you like to strip common words from the results? (Y/N)")

print(common_word)

user_output = input("\nWould you like to output these results to a file? (Y/N) ")

print(user_output)